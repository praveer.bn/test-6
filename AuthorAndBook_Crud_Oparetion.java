package Test6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.Formatter;

public class AuthorAndBook_Crud_Oparetion {
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));


    public void creatBookTable() {
        try {
            Connection connection;
            PreparedStatement preparedStatement;
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            String sql = "create table book(Bid int primary key auto_increment,B_name varchar(55),B_price int,author_id int ,foreign key (author_id) references Author(Aid))";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.execute();
            System.out.println("*************book table created***************************");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    public void creatAutherTable() {
        try {
            Connection connection;
            PreparedStatement preparedStatement;
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            String sql = "create table Author(Aid int primary key auto_increment,A_name varchar(55))";
            preparedStatement = connection.prepareStatement(sql);
            preparedStatement.execute();
            System.out.println("*************author table created***************************");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

        public void incertingValueInBook() {
            try {
                Connection connection;
                PreparedStatement preparedStatement;
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
                preparedStatement=connection.prepareStatement("insert into book( B_name, B_price,author_id)values(?,?,?) ");
                System.out.println("enter the name of the book");
                String bname= bufferedReader.readLine();
                System.out.println("enter the book price");
                int bprice=Integer.parseInt(bufferedReader.readLine());
                System.out.println("enter the author id");
                String a_name= bufferedReader.readLine();
                preparedStatement.setString(1,bname);
                preparedStatement.setInt(2,bprice);
                preparedStatement.setString(3,a_name);
                preparedStatement.executeUpdate();
                System.out.println("**********************Book_Value_inserted**************************");


            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    public void incertingValueInauthor() {
        try {
            Connection connection;
            PreparedStatement preparedStatement;
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            preparedStatement=connection.prepareStatement("insert into Author( A_name)values(?) ");
            System.out.println("enter the name of the author");
            String aname= bufferedReader.readLine();
            preparedStatement.setString(1,aname);
            preparedStatement.executeUpdate();
            System.out.println("**********************author_Value_inserted**************************");


        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public  void updatebook(){
        try{
        Connection connection;
        PreparedStatement preparedStatement;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");

        preparedStatement=connection.prepareStatement("update book set B_price=? where Bid=? ");
            System.out.println("enter the book price");
            int bprice=Integer.parseInt(bufferedReader.readLine());
            System.out.println("enter the book id");
            int bid=Integer.parseInt(bufferedReader.readLine()); ;
            preparedStatement.setInt(1,bprice);
            preparedStatement.setInt(2,bid);
            preparedStatement.execute();
            System.out.println("**************updated*************************");

    } catch (ClassNotFoundException e) {
        throw new RuntimeException(e);
    } catch (SQLException e) {
        throw new RuntimeException(e);
    } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void deletebook(){
        try{
        Connection connection;
        PreparedStatement preparedStatement;
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
            System.out.println("enter the id from which u wanna delete book data");
            int tid = Integer.parseInt(bufferedReader.readLine());
            String sql = "delete from book where Bid =?";
            preparedStatement=connection.prepareStatement(sql);
            preparedStatement.setInt(1,tid);
            preparedStatement.executeUpdate();
    } catch (ClassNotFoundException e) {
        throw new RuntimeException(e);
    } catch (SQLException e) {
        throw new RuntimeException(e);
    } catch (IOException e) {
        throw new RuntimeException(e);
    }
    }

}

