package Test6;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Author_Book_Menu {
    BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));
    boolean flag=true;
    AuthorAndBook_Crud_Oparetion authorAndBook_crud_oparetion=new AuthorAndBook_Crud_Oparetion();
    Show show=new Show();

    public  void menu() throws IOException {
        while (flag) {
            System.out.println("press 1 create table of author");
            System.out.println("press 2 create table of book");
            System.out.println("press 3 insert value in table author");
            System.out.println("press 4 insert value in table  book");
            System.out.println("press 5 for read data");
            System.out.println("press 6 for update book");
            System.out.println("press 7 for delete book");
            System.out.println("press 8 for update author");
            System.out.println("press 9 for delete author");
            System.out.println("press 0 for exit");
            System.out.println("**************************************");
            System.out.println("enter your option");
            int option=Integer.parseInt(bufferedReader.readLine());
            switch (option){
                case 0:
                    flag=false;
                    break;
                case 1:
                    authorAndBook_crud_oparetion.creatBookTable();
                    break;
                case 2:
                    authorAndBook_crud_oparetion.creatAutherTable();
                    break;
                case 3:
                    authorAndBook_crud_oparetion.incertingValueInauthor();
                    break;
                case 4:
                    authorAndBook_crud_oparetion.incertingValueInBook();
                    break;
                case 5:
                    show.showDetails();
                    break;
                case 6:
                    authorAndBook_crud_oparetion.updatebook();
                    break;
                case 7:
                    authorAndBook_crud_oparetion.deletebook();
                    break;
                default:
                    System.out.println("wrong input");
            }
        }
    }

    public static void main(String[] args) throws IOException {
        new Author_Book_Menu().menu();
    }
}
