package Test6;

import java.sql.*;
import java.util.Formatter;

public class Show {
    public void showDetails() {
            try{
                Connection connection;
                PreparedStatement preparedStatement;
                Statement statement;
                Class.forName("com.mysql.jdbc.Driver");
                connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test6", "root", "root");
                statement=connection.createStatement();
                String sql="select Author.A_name,book.B_name from Author,book where Author.Aid=book.author_id ";
                ResultSet resultSet=statement.executeQuery(sql);
                Formatter formatter = new Formatter();
                formatter.format("%15s %15s \n","Author_name","Book_name");
                while (resultSet.next()) {
                    formatter.format("%15s %15s \n", resultSet.getString(1), resultSet.getString(2));

                }
                System.out.println(formatter);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

